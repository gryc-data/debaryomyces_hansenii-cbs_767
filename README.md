## *Debaryomyces hansenii* CBS 767

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJNA13832](https://www.ebi.ac.uk/ena/browser/view/PRJNA13832)
* **Assembly accession**: [GCA_000006445.2](https://www.ebi.ac.uk/ena/browser/view/GCA_000006445.2)
* **Original submitter**: Genolevures consortium

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM644v2
* **Assembly length**: 12,152,486
* **#Chromosomes**: 7
* **Mitochondiral**: No
* **N50 (L50)**: 2,007,515 (3)

### Annotation overview

* **Original annotator**: Genolevures consortium
* **CDS count**: 6272
* **Pseudogene count**: 129
* **tRNA count**: 200
* **rRNA count**: 0
* **Mobile element count**: 48
