# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.1 (2020-10-06)

### Edited

* mobile_element DEHA2A04268t merged annotation with gene DEHA2A04290g.
* mobile_element DEHA2B12628t merged annotation with gene DEHA2B12639g.
* mobile_element DEHA2B12760t merged annotation with gene DEHA2B12771g + edited coordinates to exlude the flanking gap.
* mobile_element DEHA2B12782t merged annotation with genes DEHA2B12787g and DEHA2B12793g.
* CDS DEHA2B12787g associated with locus DEHA2B12782t.
* mobile_element DEHA2B12804t merged annotation with gene DEHA2B12815g.
* mobile_element DEHA2B12826t merged annotation with 
gene DEHA2B12837g.
* mobile_element DEHA2C11132t merged annotation with genes DEHA2C11143g and DEHA2C11165g.
* mobile_element DEHA2C11220t merged annotation with gene DEHA2C11231g.
* mobile_element DEHA2C11308t merged annotation with gene DEHA2C11319g.
* CDS DEHA2C12243g1 associated with locus DEHA2C12232t.
* CDS DEHA2C12243g2 associated with locus 
DEHA2C12232t + add /ribosomal_slippage qualifier.
* CDS DEHA2C13541g1 associated with locus DEHA2C13530t.
* CDS DEHA2C13541g2 associated with locus DEHA2C13530t + add /ribosomal_slippage qualifier.
* CDS DEHA2D06061g associated with locus
DEHA2D06050t.
* mobile_element DEHA2E06204t merged annotation with gene DEHA2E06215g.
* mobile_element DEHA2E06468t merged annotation with gene DEHA2E06479g.
* mobile_element DEHA2E06490t merged annotation with gene DEHA2E06501g.
* gene DEHA2E06523g turned into mobile_element.
* gene DEHA2E06545g turned into mobile_element.
* mobile_element DEHA2F18348t merged annotation with gene DEHA2F18359g.
* mobile_element DEHA2F18546t merged annotation with gene DEHA2F18535g.
* mobile_element DEHA2F18584t merged annotation with gene DEHA2F18590g.
* mobile_element DEHA2F18601t merged annotation with gene DEHA2F18612g.
* mobile_element DEHA2G11242t merged annotation with gene DEHA2G11275g.
* CDS DEHA2G11297g associated with locus DEHA2G11286t.
* CDS DEHA2G11539g associated with locus DEHA2G11528t.
* Building the locus hierarchy.

### Deleted

* gene DEHA2A04290g deleted: doublon with mobile_element feature.
* gene DEHA2B12639g deleted: doublon with mobile_element DEHA2B12628t.
* gene DEHA2B12771g deleted: doublon with mobile_element DEHA2B12760t in the wrong strand.
* genes DEHA2B12787g and DEHA2B12793g: doublon with mobile_element DEHA2B12782t.
* gene DEHA2B12815g deleted: doublon with mobile_element DEHA2B12804t.
* gene DEHA2B12837g deleted: doublon with mobile_element DEHA2B12826t.
* genes DEHA2C11143g and DEHA2C11165g deleted: doublon with mobile_element DEHA2C11132t.
* gene DEHA2C11231g deleted: doublon with mobile_element DEHA2C11220t.
* gene DEHA2C11319g deleted: doublon with mobile_element DEHA2C11308t.
* gene DEHA2C12243g deleted: doublon with mobile_element DEHA2C12232t.
* gene DEHA2C13541g deleted: doublon with mobile_element DEHA2C13530t.
* gene DEHA2D06061g deleted: doublon with mobile_element DEHA2D06050t.
* gene DEHA2E06215g deleted: doublon with mobile_element DEHA2E06204t.
* gene DEHA2E06248g deleted: doublon with mobile_element DEHA2E06237t.
* gene DEHA2E06301g deleted: doublon with mobile_element DEHA2E06292t in the wrong strand.
* gene DEHA2E06479g deleted: doublon with mobile_element DEHA2E06468t.
* gene DEHA2E06501g deleted: doublon with mobile_element DEHA2E06490t.
* mobile_element DEHA2E06534t deleted: overlaps 2 distinct mobile element.
* gene DEHA2F18359g deleted: doublon with mobile_element DEHA2F18348t.
* gene DEHA2F18535g deleted: doublon with mobile_element DEHA2F18546t.
* gene DEHA2F18575g deleted: spurious doublon in the wrong strand of mobile_element DEHA2F18568t.
* gene DEHA2F18590g deleted: doublon with mobile_element DEHA2F18584t.
* gene DEHA2F18612g deleted: doublon with mobile_element DEHA2F18601t.
* gene DEHA2F27159g deleted: spurious doublon in the wrong strand of mobile_element DEHA2F27137t.
* gene DEHA2G11275g deleted: doublon with mobile_element DEHA2G11242t.
* gene DEHA2G11297g deleted: doublon with mobile_element DEHA2G11286t.
* gene DEHA2G11319g deleted: doublon and dubious.
* gene DEHA2G11341g deleted: doublon and dubious.
* gene DEHA2G11473g deleted: doublon and dubious.
* gene DEHA2G11539g deleted: doublon with mobile_element DEHA2G11528t.
* gene DEHA2G11627g deleted: doublon, unconsistant overlap.
* gene DEHA2G11649g deleted: doublon with mobile_element DEHA2G11638t.

## v1.0 (2020-10-07)

### Added

* The 9 annotated chromosomes of Debaryomyces hansenii CBS 767 (source EBI, [GCA_000006445.2](https://www.ebi.ac.uk/ena/browser/view/GCA_000006445.2)).
